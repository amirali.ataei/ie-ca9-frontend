import { React, Component } from "react";
import RingLoader from "react-spinners/RingLoader";
import Card from "./Card";

class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [],
      loading: true,
    };
  }
  render() {
    if (this.state.loading)
      return (
        <div className="col-md-8">
          <div className="all-report-cards spinner-loading-home">
            <RingLoader size={150} />
          </div>
        </div>
      );
    return (
      <div className="col-md-8">
        <div className="all-report-cards">
          {this.state.cards.map((card, index) => (
            <Card key={index} card={card} />
          ))}
        </div>
      </div>
    );
  }

  async componentDidMount() {
    const apiUrl = `http://87.247.185.122:31304/reports`;
    const bearer = "Bearer " + this.getId();
    const reqOptions = {
      method: "GET",
      headers: {'Content-Type': 'application/json', 'Authorization': bearer},
    };
    const res = await fetch(apiUrl, reqOptions);
    const json = await res.json();
    setTimeout(() => {
      console.log(json.cards);
      this.setState({
        cards: json.cards,
        loading: false,
      });
    }, 2000);
  }

  getId = () => {
    return (localStorage.getItem("jwt")).toString();
  };
}

export default Report;
