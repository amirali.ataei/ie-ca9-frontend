import { React, Component } from "react";
import "./Login.css";
import logo from "../common/photos/logo.png";
import { Redirect } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
    document.body.className = 'login_page';
  }

  render() {
    const id = this.getId();
    if (id) return <Redirect to={{ pathname: "/" }} />;
    return (
      <>
          <ToastContainer
            position="top-center"
            autoClose={3000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={true}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
          <img src={logo} alt="" />
          <form className="login_form" id="login-form" onSubmit={this.handleSubmit}>
            <label>ایمیل:</label>
            <input type="text" name="email" placeholder="آدرس ایمیل" onChange={this.handleInputChange} required autoFocus/><br/><br/>
            <label>رمز عبور:</label>
            <input type="password" name="password" placeholder="رمز عبور" onChange={this.handleInputChange} required autoFocus/><br/><br/>
            <button type="submit">ورود</button>
            <br/>
            <span className="no-account">
              آیا حساب کاربری ندارید؟ <a href="/signup">عضویت</a>
            </span><br/>
            <span className="no-account">
              رمز عبور خود را فراموش کرده اید؟ <a href="/password">بازیابی</a>
            </span>
          </form><br/><br/><br/><br/><br/>
      </>
    );
  }

  handleInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const apiUrl = `http://87.247.185.122:31304/authentication`;
    const requestOptions = {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(this.state),
    };
    const response = await fetch(apiUrl, requestOptions);
    const json = await response.json();
    console.log(json.jwtToken);

    if (json.jwtToken !== "") {
      console.log(json.jwtToken);
      document.body.className = "";
      this.props.setId(json.jwtToken);
    } else toast.error("شماره دانشجويی نامعتبر!");
  };

  getId = () => {
    return (localStorage.getItem("jwt"));
  };
}

export default Login;
